import os
from fastapi import FastAPI, HTTPException, Request, Form, Body
from fastapi.responses import HTMLResponse, JSONResponse
import asyncio
import aiohttp
from starlette.responses import FileResponse 
from starlette.responses import RedirectResponse
import jwt	# pip install pyjwt
from datetime import datetime 
import requests

# Admin API key goes here
key = '66e18b27258618000154eae5:4fce042aa9609e820e166c6fa36ae65834f8ef90a595b49c6e6569981515e2f6'

# Split the key into ID and SECRET
id, secret = key.split(':')

# Prepare header and payload
iat = int(datetime.now().timestamp())

header = {'alg': 'HS256', 'typ': 'JWT', 'kid': id}
payload = {
    'iat': iat,
    'exp': iat + 5 * 60,
    'aud': '/admin/'
}

# Create the token (including decoding secret)
token = jwt.encode(payload, bytes.fromhex(secret), algorithm='HS256', headers=header)
print(token)
# Préparer les données à envoyer à l'API Ghost
data = {
    "members": [
        {
            "email": "talaron@gmail.com",
            "newsletters": [
                {
                    "id": "62db079005b5130001da777c"
                }
            ],
            "labels": [
                {
                    "name": "SiteWeb",
                    "slug": "SiteWeb"
                }
            ],
        }
    ]
}

# Make an authenticated request to create a post
url = 'https://newsletter.laraffinerie.re/ghost/api/admin/members/'
headers = {
    'Authorization': 'Ghost {}'.format(token)
}

result = requests.post(url, json=data, headers=headers)

print("Ghost Function:")
print(result)
print("----------------")