import os
import subprocess
from fastapi import FastAPI, HTTPException, Request, Form, Body
from fastapi.responses import HTMLResponse, JSONResponse
from fastapi.staticfiles import StaticFiles
from pydantic import BaseModel, EmailStr
from dotenv import load_dotenv
import asyncio
import aiohttp
from starlette.responses import FileResponse 
from starlette.responses import RedirectResponse
import re
import markdown
import jwt	# pip install pyjwt
from datetime import datetime 
import requests
from apscheduler.schedulers.background import BackgroundScheduler

###################
## env pour mailgun
###################
# Charger les variables d'environnement depuis le fichier .env
load_dotenv()
MAILGUN_API_KEY = os.getenv("MAILGUN_API_KEY")
MAILGUN_DOMAIN = os.getenv("MAILGUN_DOMAIN")
MAILGUN_SENDER = os.getenv("MAILGUN_SENDER")  # Exemple : "noreply@yourdomain.com"
DOMAIN = os.getenv("DOMAIN")

if not all([MAILGUN_API_KEY, MAILGUN_DOMAIN, MAILGUN_SENDER]):
    raise EnvironmentError("Les variables d'environnement Mailgun ne sont pas correctement définies.")
######################################

app = FastAPI()

######################################


class Donnee(BaseModel):
    email: str
    date: str 
    
###################
## inscription à un event
###################
@app.post("/api/inscription")
async def inscription(donnee: Donnee):
    """
    Route pour l'inscription. Envoie un email via l'API Mailgun.
    """
    email = donnee.email
    date = donnee.date
    
    type_evenement = "Autre"
    # Extraire le type d'événement entre crochets
    match = re.match(r'\[(.*?)\]\s*(.*)', date)
    if match:
        print("match OK")
        type_evenement = match.group(1)  # Le type d'événement (sans les crochets)
        date_sans_type = match.group(2)  # La date sans le type
        date = date_sans_type
        print(type_evenement)
    
    # Récupère le texte message "Autres"
    if type_evenement == "Chantier":
        # document Autres
        id = "inscription-chantier-participatif-xZP5xeTmI9"
    else: 
        # document Chantiers
        id = "inscription-autres-yuN9W0jGaP"
        
    try:
        # Appel de la fonction pour récupérer le document Outline
        document_outline = await getNotesText(id)
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
    
    
    # Préparer les données de l'email
    #subject = "Confirmation d'inscription"
    #text = f"Bonjour,\nVous recevez ce mail car vous êtes inscrit·e· à l'événement : {date}.\nMerci de votre inscription, à bientôt, Merci ❤️\n !"
    text = document_outline['data']['text']
    # enleve les \ sur les lignes vides
    text = re.sub(r'^[\s]*[\\]+[\s]*$', '', text, flags=re.MULTILINE)
    # remplace xxxxx par la date
    text = text.replace('xxxxx', '**' + date + '**')
    # transforme le format markdown en html
    htmlText = markdown.markdown(text)
    
    subject = document_outline['data']['title']
    

    
    data = {
        "from": MAILGUN_SENDER,
        "to": email,
        "subject": subject,
        "html": htmlText
    }

    url = f"https://api.eu.mailgun.net/v3/{MAILGUN_DOMAIN}/messages"

    try:
        async with aiohttp.ClientSession() as session:
            async with session.post(
                url,
                auth=aiohttp.BasicAuth('api', MAILGUN_API_KEY),
                data=data
            ) as response:
                if response.status != 200:
                    error_text = await response.text()
                    raise HTTPException(status_code=500, detail=f"Erreur Mailgun: {error_text}")
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


    # Log dans un fichier les inscrpitions
    try:
        # Appel de la fonction asynchrone
        result = await appendStringToNotes(f"{email}; {date}\n\n", "inscriptions-publiques-FkakptbsvW")
        return {"status": "success", "result": result}
    except HTTPException as e:
        # Gestion des exceptions levées par appendStringToNotes
        raise e
    except Exception as e:
        # Gérer d'autres exceptions générales
        raise HTTPException(status_code=500, detail=f"Erreur inattendue: {str(e)}")    


    return JSONResponse(content={"message": "Inscription réussie et email envoyé."})

###################
async def getNotesText(id: str):
    urlOutline = "https://notes.laraffinerie.re/api/documents.info"
    dataOutline = {
        "id": id
    }
    headersOutline = {
        "Content-Type": "application/json",
        "Authorization": "Bearer ol_api_vm66Ulpi94Q9nFpEpcf3cmEg9Ol2JTHe4zPSP5"
    }

    try:
        async with aiohttp.ClientSession() as session:
            async with session.post(
                urlOutline,
                json=dataOutline,
                headers=headersOutline,
            ) as response:
                if response.status != 200:
                    error_text = await response.text()
                    raise HTTPException(status_code=500, detail=f"Erreur Outline: {error_text}")
                return await response.json()
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
    
###################
async def appendStringToNotes(text: str, id: str):
    urlOutline = "https://notes.laraffinerie.re/api/documents.update"
    dataOutline = {
        "id": id,
        "text": text,
        "append": True
    }
    headersOutline = {
        "Content-Type": "application/json",
        "Authorization": "Bearer ol_api_vm66Ulpi94Q9nFpEpcf3cmEg9Ol2JTHe4zPSP5"
    }

    try:
        async with aiohttp.ClientSession() as session:
            async with session.post(
                urlOutline,
                json=dataOutline,
                headers=headersOutline,
            ) as response:
                if response.status == 200:
                    # Retourner la réponse si tout se passe bien
                    return await response.json()
                else:
                    # Lever une exception en cas de statut d'erreur
                    error_text = await response.text()
                    raise HTTPException(status_code=response.status, detail=f"Erreur Ajout dans Outline: {error_text}")

    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Erreur interne: {str(e)}")    

###################
## maj du site
###################
def update_site_task():
    """
    Fonction qui exécute 'git pull' et 'hugo' pour mettre à jour le site.
    Utilisée comme tâche planifiée.
    """
    try:
        # change les droits pour avoir le droit de faire 'git pull'
        git_config = subprocess.run(["git config --global --add safe.directory /app"], cwd="/app", shell=True, capture_output=True, text=True)
        if git_config.returncode != 0:
            raise Exception(f"Erreur lors du git config: {git_config.stderr}")
        print(git_config.stdout)
        
        # Exécuter 'git pull'
        git_pull = subprocess.run(["git pull"], cwd="/app", shell=True, capture_output=True, text=True)
        if git_pull.returncode != 0:
            raise Exception(f"Erreur lors du git pull: {git_pull.stderr}")
        print(git_pull.stdout)

        # Exécuter 'hugo' pour régénérer le site
        hugo_build = subprocess.run(["hugo --ignoreCache"], cwd="/app", shell=True, capture_output=True, text=True)
        if hugo_build.returncode != 0:
            raise Exception(f"Erreur lors de la génération du site avec Hugo: {hugo_build.stderr}")
        print(hugo_build.stdout)

    except Exception as e:
        print(f"Erreur lors de la tâche planifiée: {e}")
        
@app.post("/api/update")
async def update_site():
    """
    Route pour mettre à jour le site. Effectue un git pull et régénère le site avec Hugo.
    """
    try:
        update_site_task()

    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

    return JSONResponse(content={"message": "Site mis à jour avec succès."})


###################
## Newsletter : ghost
###################

class DonneeNewsLetter(BaseModel):
    email: str

# Route API pour la newsletter
@app.post("/api/newsletter")
async def newsletter(donnee: DonneeNewsLetter):
    """
    Route pour inscrire une adresse email à la newsletter via l'API Ghost.
    """
    email = donnee.email
    # Admin API key goes here
    key = '66e18b27258618000154eae5:4fce042aa9609e820e166c6fa36ae65834f8ef90a595b49c6e6569981515e2f6'

    # Split the key into ID and SECRET
    id, secret = key.split(':')

    # Prepare header and payload
    iat = int(datetime.now().timestamp())

    header = {'alg': 'HS256', 'typ': 'JWT', 'kid': id}
    payload = {
        'iat': iat,
        'exp': iat + 5 * 60,
        'aud': '/admin/'
    }

    # Create the token (including decoding secret)
    token = jwt.encode(payload, bytes.fromhex(secret), algorithm='HS256', headers=header)
    # Préparer les données à envoyer à l'API Ghost
    data = {
        "members": [
            {
                "email": email,
                "newsletters": [
                    {
                        "id": "62db079005b5130001da777c"
                    }
                ],
                "labels": [
                    {
                        "name": "SiteWeb",
                        "slug": "SiteWeb"
                    }
                ],
            }
        ]
    }
    
    # Make an authenticated request to create a post
    url = 'https://newsletter.laraffinerie.re/ghost/api/admin/members/'
    headers = {
        'Authorization': 'Ghost {}'.format(token)
    }

    try:
        response = requests.post(url, json=data, headers=headers)
        response.raise_for_status()  # Vérifie les erreurs HTTP
    except requests.exceptions.RequestException as e:
        raise HTTPException(status_code=500, detail=f"Erreur de requête à l'API Ghost: {str(e)}")

   # Traiter la réponse et vérifier le statut
    try:
        result = response.json()
    except ValueError:
        raise HTTPException(status_code=500, detail="Erreur lors de la conversion de la réponse en JSON")

    # Vérifier si l'API a renvoyé une erreur spécifique
    if response.status_code != 201:
        raise HTTPException(status_code=response.status_code, detail=f"Erreur de l'API Ghost: {result.get('errors', 'Unknown error')}")

    return {"message": "Membre ajouté avec succès", "data": result}


######################################
# CRON
######################################
# Initialisation du planificateur
scheduler = BackgroundScheduler()

# Planification de la tâche pour s'exécuter chaque jour à 04h00
scheduler.add_job(update_site_task, "cron", hour=4, minute=0)

# Démarrage du planificateur
scheduler.start()

@app.on_event("startup")
async def startup_event():
    """Démarre le planificateur lorsque FastAPI démarre."""
    print("Application FastAPI démarrée, planificateur activé.")

@app.on_event("shutdown")
async def shutdown_event():
    """Arrête proprement le planificateur lors de la fermeture de l'application."""
    scheduler.shutdown()
    print("Application FastAPI arrêtée, planificateur désactivé.")


###################
## site static
###################
# Monter le répertoire 'public' généré par Hugo en tant que fichiers statiques
# Il FAUT le mettre après pour ne pas foirer les routes /api d'avant
app.mount("/", StaticFiles(directory="/app/public", html=True), name="public")

