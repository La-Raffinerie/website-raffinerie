# Dockerfile

# pull the official docker image
FROM python:3-alpine

# install dependencies
COPY requirements.txt .
RUN pip install -r requirements.txt

# Install the Hugo go app.
RUN apk add --update hugo

# Install git.
RUN apk add --update git