window.bootstrap = require('bootstrap/dist/js/bootstrap.bundle.js');

const form = document.getElementById('agendaForm');
if (form) {
    form.addEventListener('submit', function(event) {
        event.preventDefault();  // Empêche le rechargement de la page

        // Récupère les valeurs du formulaire
        const email = document.getElementById('email').value;
        let dateEtType = document.getElementById('date').value;

        // Utiliser une RegExp pour capturer le texte avant le ">"
        let match = dateEtType.match(/\[(.*?)\]\s*(.*)/);
        let type;

        // Si la correspondance est trouvée
        if (match) {
            type = match[1]; // Le texte avant le ">"
            date = match[2]; // Supprime la partie "Type>"
        }

        let nomModal = "modalConfirmation";
        let nomSpan = "nomEvent";
        if (type == "Chantier") {
            nomModal = "modalConfirmationChantier";
            nomSpan = "nomEventChantier";
        }
        document.getElementById(nomSpan).textContent = date;
        let myModal = new bootstrap.Modal(document.getElementById(nomModal), {});

        // Crée un objet avec les données du formulaire
        const data = {
            email: email,
            date: dateEtType
        };

        console.log(email);
        console.log(date);
        // Envoie les données via fetch avec la méthode POST
        fetch('/api/inscription', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        })
        .then(response => response.json())
        .then(data => {
            myModal.show();
        })
        .catch(error => {
            console.error('Erreur:', error);
        });

    });
}


const formNewsletter = document.getElementById('newsletterForm');
if (formNewsletter) {
    formNewsletter.addEventListener('submit', function(event) {
        event.preventDefault();  // Empêche le rechargement de la page

        // Récupère les valeurs du formulaire
        const newsletterEmail = document.getElementById('newsletterEmail').value;


        let nomModal = "modalConfirmationNewsletter";
        let myModal = new bootstrap.Modal(document.getElementById(nomModal), {});

        // Crée un objet avec les données du formulaire
        const dataNewsletter = {
            email: newsletterEmail,
        };

        console.log(newsletterEmail);
        // Envoie les données via fetch avec la méthode POST
        fetch('/api/newsletter', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(dataNewsletter)
        })
        .then(response => response.json())
        .then(data => {
            
            myModal.show();
            // Simulate a mouse click:
            //window.location.href = "/agenda";
        })
        .catch(error => {
            console.error('Erreur:', error);
        });

    });
}